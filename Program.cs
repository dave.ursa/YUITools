﻿using System;
using System.IO;
using System.Reflection;
using Yahoo.Yui.Compressor;

namespace YUITools
{
    class YUITools
    {

        private JavaScriptCompressor jsCompressor;
        private string[] args;

        public YUITools(string[] args)
        {
            this.args = args;
            jsCompressor = new JavaScriptCompressor();
        }

        static public void Main(string[] args)
        {
            Console.WriteLine("YUI Compressor tool for replacing generated Javascript with minified file in place.");

            YUITools tools = new YUITools(args);

            tools.process();
        }

        public void process()
        {
            if (args.Length == 0)
            {
                usageInstructions();
                return;
            }

            if (Directory.Exists(args[0]))
            {
                bool recursive = hasParam("-r");
                CompressDirectory(args[0], recursive);
            }
            else
            {
                compressFileList(args);
            }
        }

        private Boolean hasParam(string param)
        {
            foreach (string ap in args)
            {
                if (ap.Equals(param))
                {
                    return true;
                }
            }
            return false;
        }

        private void usageInstructions()
        {
            Console.WriteLine("Usage 1: YuiTools directory name [-r] - to compress all .js files in directory and if(-r) sub directories.");
            Console.WriteLine("Usage 2: YuiTools file [file] [file] ... - to compress all files given.");
            Console.WriteLine("Usage 3: YuiTools  - show usage info.");
            Console.WriteLine("By Default, all files will be written over with minified versions.");
            Console.WriteLine("Other options [-n] - add '.min' to name before .js");
        }

        private void CompressDirectory(string s, Boolean recursive)
        {
            Console.WriteLine("Treating arg as directory");
            DirectoryInfo di = new DirectoryInfo(s);
            Console.Write("Compressing: ");
            ProcessDir(di, recursive);
            Console.WriteLine("...Complete");
        }

        private void ProcessDir(DirectoryInfo di, bool recursive)
        {
            Console.Write("[" + di.Name + ": ");

            ProcessFilesInDir(di);
            if (recursive)
            {
                DirectoryInfo[] dirs = di.GetDirectories();
                foreach (DirectoryInfo sdi in dirs)
                {
                    ProcessDir(sdi, recursive);
                }
                Console.Write(" :" + di.Name + "] ");
            }
        }

        private void ProcessFilesInDir(DirectoryInfo di)
        {
            FileInfo[] files = di.GetFiles("*.js");

            foreach (FileInfo fi in files)
            {
                string inName = fi.FullName;
                //if this is already minified we don't want to make a .min.min.js ...
                if (!inName.Substring(inName.Length - 7).Equals(".min.js"))
                {                    
                    string js = File.ReadAllText(inName);
                    string minjs = jsCompressor.Compress(js);
                    string opName;
                    if (hasParam("-n"))
                    {
                        opName = inName.Substring(0,inName.Length - 3) +".min.js";
                    }
                    else
                    {
                        opName = inName;
                    }
                    File.WriteAllText(opName, minjs);
                    Console.Write(fi.Name + " ");
                }
            }
        }

        private void compressFileList(string[] args)
        {
            Console.WriteLine("Treating args as file list");
            foreach (string inName in args)
            {
                if (File.Exists(inName))
                {
                    string js = File.ReadAllText(inName);
                    string minjs = jsCompressor.Compress(js);
                    string opName;
                    if (hasParam("-n"))
                    {
                        opName = inName.Substring(0, inName.Length - 3) + ".min.js";
                    }
                    else
                    {
                        opName = inName;
                    }
                    File.WriteAllText(inName, minjs);
                }
                else
                {
                    Console.Write("File either does not exist or is a directory: "+ inName);
                }
            }
        }
    }
}
