var Zounds;
(function (Zounds_1) {
    var ZampleInner = (function () {
        function ZampleInner(url, play, playWhenLoaded) {
            this.url = url;
            this.play = play;
            this.playWhenLoaded = playWhenLoaded;
            this.loadComplete = false;
        }
        return ZampleInner;
    }());
    var Zounds = (function () {
        function Zounds() {
            this.loading = 0;
            this.loaded = 0;
            this.loadedCallback = function () { };
            this.sampleDict = {};
            try {
                window["AudioContext"] = window["AudioContext"] || window["webkitAudioContext"];
                this.context = new AudioContext();
            }
            catch (e) {
                console.log("No Audio. Exception: " + e);
            }
        }
        Zounds.prototype.loadSet = function (urlList) {
            var samples = [];
            for (var _i = 0, urlList_1 = urlList; _i < urlList_1.length; _i++) {
                var url = urlList_1[_i];
                samples[samples.length] = this.load(url);
            }
            return samples;
        };
        Zounds.prototype.load = function (url, playWhenLoaded) {
            var _this = this;
            if (playWhenLoaded === void 0) { playWhenLoaded = false; }
            if (this.context === undefined) {
                return;
            }
            this.loading++;
            var sample = new ZampleInner(url, function () { _this.playURL(url); }, playWhenLoaded);
            this.sampleDict[url] = sample;
            var req = new XMLHttpRequest();
            req.open('GET', url, true);
            req.responseType = 'arraybuffer';
            req.onload = function (ev) {
                if (req.status == 200) {
                    _this.context.decodeAudioData(req.response, function (buffer) {
                        sample.buffer = buffer;
                        sample.loadComplete = true;
                        if (sample.playWhenLoaded) {
                            sample.play();
                        }
                        _this.loaded++;
                        if (_this.loading == _this.loaded) {
                            _this.loadedCallback(true, "");
                            _this.loadedCallback = function () { };
                        }
                    });
                }
                else {
                    _this.loadedCallback(false, req.statusText + ": " + url);
                    throw new Error(req.statusText + ": " + url);
                }
            };
            req.onerror = function () {
                throw new Error("Network Error");
            };
            req.send();
            return sample;
        };
        Zounds.prototype.loadComplete = function () {
            if (this.context === undefined) {
                return true;
            }
            return (this.loading == this.loaded);
        };
        Zounds.prototype.callBackWhenComplete = function (cback) {
            this.loadedCallback = cback;
            if (this.context === undefined) {
                cback(false, "Sound not supported.");
            }
            if (this.loading == this.loaded) {
                this.loadedCallback(true, "");
                this.loadedCallback = function () { };
            }
            else {
            }
        };
        Zounds.prototype.getSamplesStillLoading = function () {
            var rval = [];
            for (var url in this.sampleDict) {
                if (!this.sampleDict[url].loadComplete) {
                    rval[rval.length] = url;
                }
            }
            return rval;
        };
        Zounds.prototype.play = function (sound) {
            if (this.context === undefined) {
                return;
            }
            this.playURL(sound.url);
        };
        Zounds.prototype.playURL = function (url) {
            if (this.context === undefined) {
                return;
            }
            var samp = this.sampleDict[url];
            if (samp === undefined) {
                this.load(url, true);
                return;
            }
            if (!samp.loadComplete) {
                samp.playWhenLoaded = true;
                return;
            }
            var source = this.context.createBufferSource();
            source.connect(this.context.destination);
            source.buffer = samp.buffer;
            source.start();
        };
        return Zounds;
    }());
    Zounds_1.Zounds = Zounds;
})(Zounds || (Zounds = {}));
